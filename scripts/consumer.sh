#!/bin/sh

# just in case ...
mkdir -p ${MOUNT_POINT}
gcsfuse --stat-cache-ttl 0m5s ${BUCKET} ${MOUNT_POINT}

while true
do
  until [ -f "${MOUNT_POINT}/message" ]
  do
    echo wait for producer
    sleep 2
  done
  ls -l ${MOUNT_POINT}
  cat ${MOUNT_POINT}/message
  sleep 5
done