include envs
export

help: ## this info
	@# adapted from https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
	@echo '_________________'
	@echo '| Make targets: |'
	@echo '-----------------'
	@cat Makefile | grep -E '^[a-zA-Z_-]+:.*?## .*$$' | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## build gcsfuse image
	docker-compose build

up: ## bring system up, see docker-compose.yml
	@if ! docker images gcsfuse | grep -q gcsfuse ; \
	then \
		make build; \
	fi
	docker-compose up -d --force-recreate --remove-orphans

down: ## bring system dev-down, see docker-compose.yml
	docker-compose down --remove-orphans --volumes

restart: ## restart test system
	docker-compose restart

follow: ## docker-compose logs -f
	docker-compose logs -f

prune: ## prune local docker containers and images
	docker container prune -f
	docker image prune -f
	docker volume prune -f

auth: ## Activate gclound service account
	gcloud auth login
	#gcloud auth activate-service-account --key-file keys/service-account.json

bucket:  ## Create the bucket if not exists
	@if ! gsutil ls -p ${GCP_PROJECT_ID} gs://${BUCKET} &> /dev/null; \
	then \
		echo creating gs://${BUCKET} ... ; \
		gsutil mb -p ${GCP_PROJECT_ID} -c regional -l ${GCP_REGION} gs://${BUCKET}; \
		sleep 10; \
	fi

destroy-bucket: ## Destroy the bucket if exists
	@if gsutil ls -p ${GCP_PROJECT_ID} gs://${BUCKET} &> /dev/null; \
	then \
		echo destroy gs://${BUCKET} ... ; \
		gsutil rb -f gs://${BUCKET}; \
	fi

sync: ## Rsync the gcs bucket
		gsutil -m rsync -r -d ./bucket gs://${BUCKET}

sync-docker-time: ## sync docker vm time with hardware clock
	@docker run --rm --privileged alpine hwclock -s

.PHONY: build prune push pull help up down restart follow
.PHONY: bucket destroy-bucket sync-docker-time