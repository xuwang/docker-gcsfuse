#!/bin/sh

# just in case ...
mkdir -p ${MOUNT_POINT}
gcsfuse --stat-cache-ttl 0m5s ${BUCKET} ${MOUNT_POINT}

while true
do
	cat <<EOF >${MOUNT_POINT}/message
======================================================
This is a message from producer on $(date)
======================================================
EOF
	echo message created
	sleep 5
done