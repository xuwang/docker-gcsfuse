#!/bin/sh

# just in case ...
mkdir -p ${MOUNT_POINT}
chown -R ${UID}:${UID} ${MOUNT_POINT}

# run with given user
if [ -f "${GCS_CREDENTIALS}" ]
then
  sudo -u "#${UID}" gcsfuse  --key-file ${GCS_CREDENTIALS} \
    -o nonempty  ${BUCKET} ${MOUNT_POINT}
else
  sudo -u "#${UID}" gcsfuse \
    -o nonempty  ${BUCKET} ${MOUNT_POINT}
fi