# Docker Example - Use [gcsfuse]() volume in containers

## Prerequisites

#### Get GCS credentials
This example will require GCS credentials. The easiest way
to set up your credentials for testing is to run the [gcloud tool][]:

```
$ gcloud auth login
```

Alternatively, you can set the GOOGLE_APPLICATION_CREDENTIALS environment variable to the path to a JSON key file downloaded from the Google Developers Console:

```
$ export GOOGLE_APPLICATION_CREDENTIALS=/path/to/key.json
```


#### Set the default GCP project:

Edit envs file, set the GCP_PROJECT_ID to your GCP project id.

```
$ source envs; gcloud config set project ${GCP_PROJECT_ID}
```

#### Create the bucket

Before invoking gcsfuse, you must have a GCS bucket that you want to mount. If
your bucket doesn't yet exist, create one:

```
$ make bucket
```

## Build the gcsfuse docker image

```
$ make build
```

## See it works

```
$ make up follow
...
producer_1  | message created
consumer_1  | total 1
consumer_1  | -rw-r--r-- 1 root root   0 May  1 00:34 index.html
consumer_1  | -rw-r--r-- 1 root root 174 May  2 03:08 message
consumer_1  | -rw-r--r-- 1 root root   0 May  1 01:15 test.html
consumer_1  | ======================================================
consumer_1  | This is a message from producer on Tue May  2 03:08:19 UTC 2017
consumer_1  | ======================================================
...
```

Go to [GCS Console] upload/delete some files to see changes in local logs.

## Shut it down

```
$ make down
```

[GCS Console]: https://console.cloud.google.com/storage
[gcsfulse]: https://github.com/GoogleCloudPlatform/gcsfuse
[console]: https://console.developers.google.com
[gcloud tool]: https://cloud.google.com/sdk/gcloud/
[app-default-credentials]: https://developers.google.com/identity/protocols/application-default-credentials#howtheywork




